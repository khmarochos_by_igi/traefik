resource "kubernetes_namespace" "longhorn" {
  metadata {
    name = "longhorn"
  }
}

resource "helm_release" "longhorn" {
  name       = "longhorn"
  repository = "https://charts.longhorn.io"
  chart      = "longhorn"
  version    = "1.4.2"
  namespace  = "longhorn"
  depends_on = [
    kubernetes_namespace.longhorn,
    aws_eks_node_group.cool
  ]
}