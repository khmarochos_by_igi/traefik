resource "aws_vpc" "vpc" {
  cidr_block           = local.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = local.env
  }

}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = local.env
  }

}
resource "aws_default_route_table" "pub_rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${local.env}-pub"
  }

}

resource "aws_subnet" "pub_sub" {
  count                   = length(data.aws_availability_zones.working.names)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index)
  availability_zone       = data.aws_availability_zones.working.names[count.index]
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${local.env}-pub"
    AZ   = data.aws_availability_zones.working.names[count.index]
  }

}

resource "aws_route_table_association" "pub_sub_assciation" {
  count          = length(data.aws_availability_zones.working.names)
  subnet_id      = element(aws_subnet.pub_sub.*.id, count.index)
  route_table_id = aws_default_route_table.pub_rt.id
}

resource "aws_eip" "nat_gw_ip" {
  # vpc = true

  tags = {
    Name      = "${local.env}-nat-gw"
    Manage_by = "Terraform"
  }
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_gw_ip.id
  subnet_id     = aws_subnet.pub_sub.0.id
  tags = {
    Name = local.env
  }

}

resource "aws_route_table" "priv_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }
  tags = {
    Name = "${local.env}-priv"
  }

}

resource "aws_subnet" "priv_sub" {
  count                   = length(data.aws_availability_zones.working.names)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index + 10)
  availability_zone       = data.aws_availability_zones.working.names[count.index]
  map_public_ip_on_launch = "false"
  tags = {
    Name = "${local.env}-priv"
    AZ   = data.aws_availability_zones.working.names[count.index]
  }

}

resource "aws_route_table_association" "priv_sub_assciation" {
  count          = length(data.aws_availability_zones.working.names)
  subnet_id      = element(aws_subnet.priv_sub.*.id, count.index)
  route_table_id = aws_route_table.priv_rt.id
}

resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "key" {
  key_name   = "cool"
  public_key = tls_private_key.key.public_key_openssh
}

resource "local_file" "foo" {
  content         = tls_private_key.key.private_key_openssh
  filename        = "./cool.pem"
  file_permission = "0400"
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow ALL"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "bastion" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = "t3.micro"
  subnet_id       = aws_subnet.pub_sub[2].id
  security_groups = [aws_security_group.allow_all.id]
  key_name        = aws_key_pair.key.id
  tags = {
    Name = "bastion"
  }
}