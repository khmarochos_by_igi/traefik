resource "aws_eks_cluster" "cool" {
  name                      = "cool"
  role_arn                  = aws_iam_role.eks_cluster_role.arn
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  vpc_config {
    subnet_ids = setsubtract(aws_subnet.pub_sub.*.id, [aws_subnet.pub_sub[4].id])
  }
}

resource "aws_eks_node_group" "cool" {
  cluster_name    = aws_eks_cluster.cool.name
  node_group_name = "cool"
  node_role_arn   = aws_iam_role.eks_node_group_role.arn
  subnet_ids      = aws_subnet.priv_sub.*.id
  disk_size       = 30
  remote_access {
    ec2_ssh_key = aws_key_pair.key.id
    #source_security_group_ids = [aws_security_group.allow_all.id]
  }

  scaling_config {
    desired_size = 4
    max_size     = 4
    min_size     = 4
  }


}

resource "aws_eks_addon" "coredns" {
  depends_on = [aws_eks_node_group.cool]

  cluster_name = aws_eks_cluster.cool.name
  addon_name   = "coredns"
  //addon_version     = "v1.8.7-eksbuild.3" #e.g., previous version v1.8.7-eksbuild.2 and the new version is v1.8.7-eksbuild.3
  resolve_conflicts = "OVERWRITE"

}

resource "aws_eks_addon" "kube_proxy" {
  depends_on = [aws_eks_node_group.cool]

  cluster_name = aws_eks_cluster.cool.name
  addon_name   = "kube-proxy"
  //addon_version     = "v1.8.7-eksbuild.3" #e.g., previous version v1.8.7-eksbuild.2 and the new version is v1.8.7-eksbuild.3
  resolve_conflicts = "OVERWRITE"
}

resource "aws_eks_addon" "vpc_cni" {
  depends_on = [aws_eks_node_group.cool]

  cluster_name = aws_eks_cluster.cool.name
  addon_name   = "vpc-cni"
  //addon_version     = "v1.8.7-eksbuild.3" #e.g., previous version v1.8.7-eksbuild.2 and the new version is v1.8.7-eksbuild.3
  resolve_conflicts = "OVERWRITE"
}







