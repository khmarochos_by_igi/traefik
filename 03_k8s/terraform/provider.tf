provider "aws" {
  region  = "us-east-1"
  profile = "acloudguru"

}


provider "kubernetes" {
  host                   = aws_eks_cluster.cool.endpoint
  cluster_ca_certificate = base64decode("${aws_eks_cluster.cool.certificate_authority[0].data}")
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = ["eks", "get-token", "--cluster-name", aws_eks_cluster.cool.name]
    command     = "aws"
    env         = { AWS_PROFILE = "acloudguru" }
  }
}

provider "helm" {
  kubernetes {
    host                   = aws_eks_cluster.cool.endpoint
    cluster_ca_certificate = base64decode("${aws_eks_cluster.cool.certificate_authority[0].data}")
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", aws_eks_cluster.cool.name]
      command     = "aws"
      env         = { AWS_PROFILE = "acloudguru" }
    }
  }

}