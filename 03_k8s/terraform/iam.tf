resource "aws_iam_role" "eks_cluster_role" {
  name = "eksClusterRole" #"eks-cluster-role"

  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonEKSClusterPolicy", "arn:aws:iam::aws:policy/AdministratorAccess"]


  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "eks.amazonaws.com"
        }
      },
    ]
  })
}


# resource "aws_iam_role" "eks_fargate_pod_role" {
#   name                = "eks-fargate-pod-execution-role"
#   managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"]


#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Sid    = ""
#         Principal = {
#           Service = "eks-fargate-pods.amazonaws.com"
#         }
#       },
#     ]
#   })
# }


resource "aws_iam_role" "eks_node_group_role" {
  name                = "eks-node-group-role"
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy", "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly", "arn:aws:iam::aws:policy/AdministratorAccess"]


  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
    }
  )
}

# resource "aws_iam_role" "secrets_reader" {
#   name = var.external_secret_role_name
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17",
#     Statement = [
#       {
#         Effect = "Allow",
#         Principal = {
#           Federated = aws_iam_openid_connect_provider.cool.id
#         },
#         Action = "sts:AssumeRoleWithWebIdentity",
#         Condition = {
#           StringEquals = {
#             "${aws_iam_openid_connect_provider.cool.url}:sub" = "system:serviceaccount:argocd:${var.external_secret_role_name}",
#             "${aws_iam_openid_connect_provider.cool.url}:aud" = "sts.amazonaws.com"
#           }
#         }
#       }
#     ]
#   })
#   inline_policy {
#     name = "secrets_read"

#     policy = jsonencode({
#       Version = "2012-10-17",
#       Statement = [
#         {
#           Sid    = "VisualEditor0",
#           Effect = "Allow",
#           Action = [
#             "secretsmanager:GetSecretValue",
#             "secretsmanager:DescribeSecret"
#           ],
#           Resource = "*"
#         }
#       ]
#     })
#   }
# }

