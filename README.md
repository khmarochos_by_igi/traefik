# Traefik in docker
## Requermens:
- Ubuntu 22.04
- docker
- docker compose
## Install docker (compose)
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
systemctl enable docker
systemctl start docker
apt install apache2-utils
docker swarm init


## Traefik setting
### Entrypoints setup
https://doc.traefik.io/traefik/routing/entrypoints/

### metrics
https://doc.traefik.io/traefik/observability/metrics/overview/
https://doc.traefik.io/traefik/observability/metrics/prometheus/

### logs
https://doc.traefik.io/traefik/observability/logs/
https://doc.traefik.io/traefik/observability/access-logs/

### tracing
https://doc.traefik.io/traefik/observability/tracing/overview/ 



sudo yum install -y device-mapper-multipath iscsi-initiator-utils

helm repo add longhorn https://charts.longhorn.io
helm repo update
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --version 1.4.2
kubectl -n longhorn-system get pod

curl https://whoami2.mos-eisley.online -H "X-Canary-Header: knock-knock"

https://doc.traefik.io/traefik/routing/routers/#priority


aws eks update-kubeconfig --name cool --profile acloudguru


helm repo add jetstack https://charts.jetstack.io

helm repo update

kubectl create namespace cert-manager

kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.crds.yaml

helm install cert-manager jetstack/cert-manager --namespace cert-manager --values=values.yaml --version v1.9.1
