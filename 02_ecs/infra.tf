resource "aws_lb_target_group" "http80" {
  name                 = "http80"
  port                 = 80
  protocol             = "TCP"
  target_type          = "ip"
  vpc_id               = data.aws_vpc.default.id
  deregistration_delay = 15
  health_check {
    enabled             = true
    healthy_threshold   = 2
    interval            = 5
    protocol            = "TCP"
    timeout             = 5
    unhealthy_threshold = 2
  }
  tags = {
    Manage_by = "Terraform"
  }
}

resource "aws_lb_target_group" "http443" {
  name                 = "http443"
  port                 = 443
  protocol             = "TCP"
  target_type          = "ip"
  vpc_id               = data.aws_vpc.default.id
  deregistration_delay = 15
  health_check {
    enabled             = true
    healthy_threshold   = 2
    interval            = 5
    protocol            = "TCP"
    timeout             = 5
    unhealthy_threshold = 2
  }
  tags = {
    Manage_by = "Terraform"
  }
}

resource "aws_lb_target_group" "http8080" {
  name                 = "http8080"
  port                 = 8080
  protocol             = "TCP"
  target_type          = "ip"
  vpc_id               = data.aws_vpc.default.id
  deregistration_delay = 15
  health_check {
    enabled             = true
    healthy_threshold   = 2
    interval            = 5
    protocol            = "TCP"
    timeout             = 5
    unhealthy_threshold = 2
  }
  tags = {
    Manage_by = "Terraform"
  }
}

resource "aws_security_group" "allow_all" {
  name        = "allow-all"
  description = "Allow ALL"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Manage_by = "Terraform"
  }
}

resource "aws_lb" "cool" {
  name               = "cool"
  internal           = false
  load_balancer_type = "network"
  #security_groups    = [aws_security_group.allow_all.id]
  subnets = data.aws_subnets.default.ids

  tags = {
    Manage_by = "Terraform"
  }
}

resource "aws_route53_record" "record" {
  zone_id = data.aws_route53_zone.selected.id
  name    = ""
  type    = "A"

  alias {
    name                   = aws_lb.cool.dns_name
    zone_id                = aws_lb.cool.zone_id
    evaluate_target_health = true
  }
}

resource "aws_lb_listener" "http80" {
  load_balancer_arn = aws_lb.cool.arn
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.http80.arn
  }
}

resource "aws_lb_listener" "http443" {
  load_balancer_arn = aws_lb.cool.arn
  port              = "443"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.http443.arn
  }
}


resource "aws_lb_listener" "http8080" {
  load_balancer_arn = aws_lb.cool.arn
  port              = "8080"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.http8080.arn
  }
}

resource "aws_cloudwatch_log_group" "ecs" {
  name = "/ecs/traefik"


}


resource "aws_ecs_task_definition" "traefik" {
  family                   = "traefik_lb"
  task_role_arn            = aws_iam_role.ecs.arn
  execution_role_arn       = aws_iam_role.ecs.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions = jsonencode([
    {
      name  = "traefik"
      image = "traefik:v2.10"
      # cpu              = 256
      # memory           = 512
      essential = true
      command = [
        "--api.insecure=true",
        "--providers.ecs.clusters=cool",
        "--providers.ecs.refreshSeconds=15",
        "--entryPoints.http.address=:80",
        "--entryPoints.https.address=:443",
        "--metrics.prometheus=true",
        "--entryPoints.metrics.address=:8082",
        "--metrics.prometheus.entryPoint=metrics",
        "--log=true",
        "--log.level=DEBUG",
        "--accesslog=true",
        "--certificatesresolvers.le.acme.httpchallenge=true",
        "--certificatesresolvers.le.acme.httpchallenge.entrypoint=http",
        "--certificatesresolvers.le.acme.email=shalva321@dminutesfb.com" #,
        #"--certificatesresolvers.le.acme.storage=/letsencrypt/acme.json"
      ],
      "portMappings" : [
        {
          "name" : "traefik-80-tcp",
          "containerPort" : 80,
          "hostPort" : 80,
          "protocol" : "tcp",
          "appProtocol" : "http"
        },
        {
          "name" : "traefik-8080-tcp",
          "containerPort" : 8080,
          "hostPort" : 8080,
          "protocol" : "tcp",
          "appProtocol" : "http"
        },
        {
          "name" : "traefik-443-tcp",
          "containerPort" : 443,
          "hostPort" : 443,
          "protocol" : "tcp",
          "appProtocol" : "http"
        }
      ],
      "logConfiguration" : {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "/ecs/traefik",
          "awslogs-region" : "us-east-1",
          "awslogs-stream-prefix" : "ecs"
        }
      },
      "dockerLabels" : {
        "traefik.enable" : "true",
        "traefik.http.middlewares.https-redirect.redirectscheme.permanent" : "true",
        "traefik.http.middlewares.https-redirect.redirectscheme.scheme" : "https",
        "traefik.http.services.admin.loadbalancer.server.port" : "8080",
        "traefik.http.routers.admin.service" : "admin",
        "traefik.http.routers.admin.rule" : "Host(`admin.${var.zone}`)",
        "traefik.http.routers.admin.entrypoints" : "http",
        "traefik.http.routers.admin.middlewares" : "https-redirect",
        "traefik.http.routers.admin-https.service" : "admin",
        "traefik.http.routers.admin-https.rule" : "Host(`admin.${var.zone}`)"
        "traefik.http.routers.admin-https.entrypoints" : "https",
        "traefik.http.routers.admin-https.tls" : "true",
        "traefik.http.routers.admin-https.tls.certresolver" : "le",
        "traefik.http.services.mon.loadbalancer.server.port" : "8082",
        "traefik.http.routers.traefik-mon.service" : "mon",
        "traefik.http.routers.traefik-mon.rule" : "Host(`mon.${var.zone}`)",
        "traefik.http.routers.traefik-mon.entrypoints" : "https",
        "traefik.http.routers.traefik-mon.tls" : "true",
        "traefik.http.routers.traefik-mon.tls.certresolver" : "le",
        "traefik.http.middlewares.traefik-mon.replacepath.path" : "/metrics",
        "traefik.http.routers.traefik-mon.middlewares" : "traefik-mon"
      }
    },

  ])
}

resource "aws_route53_record" "record_admin" {
  zone_id = data.aws_route53_zone.selected.id
  name    = "admin"
  type    = "A"

  alias {
    name                   = aws_lb.cool.dns_name
    zone_id                = aws_lb.cool.zone_id
    evaluate_target_health = true
  }
}

# resource "aws_route53_record" "record_mon" {
#   zone_id = data.aws_route53_zone.selected.id
#   name    = "mon"
#   type    = "A"

#   alias {
#     name                   = aws_lb.cool.dns_name
#     zone_id                = aws_lb.cool.zone_id
#     evaluate_target_health = true
#   }
# }

resource "aws_route53_record" "record_asterisc" {
  zone_id = data.aws_route53_zone.selected.id
  name    = "*"
  type    = "A"

  alias {
    name                   = aws_lb.cool.dns_name
    zone_id                = aws_lb.cool.zone_id
    evaluate_target_health = true
  }
}

resource "aws_ecs_cluster" "cool" {
  name = "cool"
}


resource "aws_ecs_service" "traefik" {
  name            = "traefik-lb"
  cluster         = aws_ecs_cluster.cool.id
  task_definition = aws_ecs_task_definition.traefik.arn
  desired_count   = 1
  launch_type     = "FARGATE"
  #iam_role        = aws_iam_role.foo.arn
  #depends_on      = [aws_iam_role_policy.foo]
  network_configuration {
    subnets          = data.aws_subnets.default.ids
    security_groups  = [aws_security_group.allow_all.id]
    assign_public_ip = true
  }


  load_balancer {
    target_group_arn = aws_lb_target_group.http8080.arn
    container_name   = "traefik"
    container_port   = 8080
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.http443.arn
    container_name   = "traefik"
    container_port   = 443
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.http80.arn
    container_name   = "traefik"
    container_port   = 80
  }


}

resource "aws_cloudwatch_log_group" "whoami" {
  name = "/ecs/whoami"


}

resource "aws_ecs_task_definition" "whoami" {
  family                   = "whoami"
  task_role_arn            = aws_iam_role.ecs.arn
  execution_role_arn       = aws_iam_role.ecs.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions = jsonencode([
    {
      name      = "whoami"
      image     = "traefik/whoami"
      essential = true
      "portMappings" : [
        {
          "name" : "traefik-80-tcp",
          "containerPort" : 80,
          "hostPort" : 80,
          "protocol" : "tcp",
          "appProtocol" : "http"
        }
      ],
      "logConfiguration" : {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "/ecs/whoami",
          "awslogs-region" : "us-east-1",
          "awslogs-stream-prefix" : "ecs"
        }
      },
      "dockerLabels" : {
        "traefik.enable" : "true",
        "traefik.http.services.whoami.loadbalancer.server.port" : "80"
        "traefik.http.services.whoami.loadbalancer.sticky.cookie" : "true" #sticky-session
        "traefik.http.routers.whoami.rule" : "Host(`whoami.${var.zone}`)"
        "traefik.http.routers.whoami.entrypoints" : "http"
        "traefik.http.routers.whoami.middlewares" : "https-redirect"
        "traefik.http.routers.whoami-https.rule" : "Host(`whoami.${var.zone}`)"
        "traefik.http.routers.whoami-https.entrypoints" : "https"
        "traefik.http.routers.whoami-https.tls" : "true"
        "traefik.http.routers.whoami-https.tls.certresolver" : "le"

      }
    },

  ])
}

resource "aws_ecs_service" "whoami" {
  name            = "whoami"
  cluster         = aws_ecs_cluster.cool.id
  task_definition = aws_ecs_task_definition.whoami.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    subnets          = data.aws_subnets.default.ids
    security_groups  = [aws_security_group.allow_all.id]
    assign_public_ip = true
  }
}


resource "aws_cloudwatch_log_group" "web" {
  name = "/ecs/web"


}

resource "aws_ecs_task_definition" "web" {
  family                   = "web"
  task_role_arn            = aws_iam_role.ecs.arn
  execution_role_arn       = aws_iam_role.ecs.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions = jsonencode([
    {
      name      = "web"
      image     = "nginx"
      essential = true
      "portMappings" : [
        {
          "name" : "traefik-80-tcp",
          "containerPort" : 80,
          "hostPort" : 80,
          "protocol" : "tcp",
          "appProtocol" : "http"
        }
      ],
      "logConfiguration" : {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "/ecs/web",
          "awslogs-region" : "us-east-1",
          "awslogs-stream-prefix" : "ecs"
        }
      },
      "dockerLabels" : {
        "traefik.enable" : "true",
        "traefik.http.services.web.loadbalancer.server.port" : "80"
        "traefik.http.routers.web.rule" : "Host(`web.${var.zone}`)"
        "traefik.http.routers.web.entrypoints" : "http"
        "traefik.http.routers.web.middlewares" : "https-redirect"
        "traefik.http.routers.web-https.rule" : "Host(`web.${var.zone}`)"
        "traefik.http.routers.web-https.entrypoints" : "https"
        "traefik.http.routers.web-https.tls" : "true"
        "traefik.http.routers.web-https.tls.certresolver" : "le"

      }
    },

  ])
}

resource "aws_ecs_service" "web" {
  name            = "web"
  cluster         = aws_ecs_cluster.cool.id
  task_definition = aws_ecs_task_definition.web.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets          = data.aws_subnets.default.ids
    security_groups  = [aws_security_group.allow_all.id]
    assign_public_ip = true
  }
}