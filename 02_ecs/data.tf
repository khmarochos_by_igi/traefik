data "aws_vpc" "default" {}

data "aws_subnets" "default" {}



variable "zone" {
  default = "027335897078.realhandsonlabs.net"
}

data "aws_route53_zone" "selected" {
  name = var.zone
}

output "name" {
  value = data.aws_route53_zone.selected.id
}