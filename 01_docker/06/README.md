mkdir certs
## Generate CA
### Generate RSA
openssl genrsa -aes256 -out certs/root-ca-key.pem 4096
### Generate a public CA Cert
openssl req -new -x509 -sha256 -days 365 -key certs/root-ca-key.pem -out certs/root-ca.pem 
## Generate Certificate
### Create a RSA key
openssl genrsa -out certs/admin.local-key.pem 4096

### Create a Certificate Signing Request (CSR)
openssl req -new -sha256  -key certs/admin.local-key.pem -out certs/admin-cert.csr
### conf
echo "subjectAltName=DNS:admin.uk.local" > certs/extfile.cnf
### Create the certificate
openssl x509 -req -sha256 -days 365 -in certs/admin-cert.csr -CA certs/root-ca.pem -CAkey certs/root-ca-key.pem -out certs/admin-cert.pem  -extfile certs/extfile.cnf -CAcreateserial 
### fullchain
cat certs/admin-cert.pem > certs/fullchain-admin.pem
cat certs/root-ca.pem >> certs/fullchain-admin.pem


## make certs 
### linux
cp certs/root-ca.pem /usr/local/share/ca-certificates/root-ca.crt
sudo update-ca-certificates

## Generate Certificate
### Create a RSA key
openssl genrsa -out certs/web.local-key.pem 4096
### Create a Certificate Signing Request (CSR)
openssl req -new -sha256  -key certs/web.local-key.pem -out certs/web-cert.csr
### conf
echo "subjectAltName=DNS:web.uk.local" > certs/extfile.cnf
### Create the certificate
openssl x509 -req -sha256 -days 365 -in certs/web-cert.csr -CA certs/root-ca.pem -CAkey certs/root-ca-key.pem -out certs/web-cert.pem  -extfile certs/extfile.cnf -CAcreateserial 
### fullchain
cat certs/web-cert.pem > certs/fullchain-web.pem
cat certs/root-ca.pem >> certs/fullchain-web.pem




## Generate Certificate
### Create a RSA key
openssl genrsa -out certs/whoami.local-key.pem 4096
### Create a Certificate Signing Request (CSR)
openssl req -new -sha256  -key certs/whoami.local-key.pem -out certs/whoami-cert.csr
### conf
echo "subjectAltName=DNS:whoami.uk.local" > certs/extfile.cnf
### Create the certificate
openssl x509 -req -sha256 -days 365 -in certs/whoami-cert.csr -CA certs/root-ca.pem -CAkey certs/root-ca-key.pem -out certs/whoami-cert.pem  -extfile certs/extfile.cnf -CAcreateserial 
### fullchain
cat certs/whoami-cert.pem > certs/fullchain-whoami.pem
cat certs/root-ca.pem >> certs/fullchain-whoami.pem