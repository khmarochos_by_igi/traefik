output "ssh" {
  value = "ssh -i ${local_file.key.filename} ubuntu@${aws_instance.web.public_ip}"
}